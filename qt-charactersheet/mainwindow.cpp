#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    character = loadsheet("data/sheet.txt");
    loadstats("data", &character);
    this->updatecharactername(character.name);
    this->updatecharactersaves();
    this->updatelevel();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::updatecharactername(QString newname) {
    this->ui->charactername->setText(newname);
}

void MainWindow::updatelevel() {
    this->ui->leveldisplay->setText("Level " + QString::number(character.level) + " " + superclassArray[character.superclass]);
}

void MainWindow::updatecharactersaves() {
    int lvl=this->character.level;
    this->ui->ppdm_saveval->setText(QString::number(character.ppdm[lvl]));
    this->ui->rsw_saveval->setText(QString::number(character.rsw[lvl]));
    this->ui->pp_saveval->setText(QString::number(character.pp[lvl]));
    this->ui->bw_saveval->setText(QString::number(character.bw[lvl]));
    this->ui->spell_saveval->setText(QString::number(character.spell[lvl]));
}
