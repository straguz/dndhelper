#ifndef FILES_H
#define FILES_H

#include <QTextStream>
#include <QFile>
#include <QStringList>
#include <QRegularExpression>
#include <character.h>

// Debug/helper function to print a string to console with printf
void qsprintln(QString string);

// Function to load in character sheet "saved file"
playercharacter loadsheet(QString path);

// Function to load in player superclass stats
void loadstats(QString path, playercharacter* player);

#endif // FILES_H
