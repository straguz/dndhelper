#include "mainwindow.h"
#include "files.h"
#include "character.h"

#include <QApplication>
#include <stdio.h>

int main(int argc, char *argv[])
{
    //const char* fpath = "data/wizard-stats.txt";
    //struct classStruct classdata;
    //int b = loadClassData(fpath, &classdata);

    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
