#ifndef CHARACTER_H
#define CHARACTER_H
#include <QString>

// Constants
enum superclasses {warrior, rogue, wizard, priest};
const QString superclassArray[] = {"Warrior", "Rogue", "Wizard", "Priest"};
const int levelrange = 21;

// Player character struct with all the base player stats
struct playercharacter {
    QString name;
    superclasses superclass;
    QString superclassname;
    int level;
    int thaco[levelrange];
    int ppdm[levelrange];
    int rsw[levelrange];
    int pp[levelrange];
    int bw[levelrange];
    int spell[levelrange];
};

#endif // CHARACTER_H
