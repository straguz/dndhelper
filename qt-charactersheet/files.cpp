// Central file for handling all character sheet related file IO.
// TODO Need to write up documentation describing the various files involved.
// Should have the following:
//  Global files:
//      - stats.txt: File which containes progression of all saving throws,
//                   THAC0, etc. for each class.
//      - spells.txt: Data on all spells in the game, their stats, etc.
//  Per-character files:
//      - sheet.txt: Main character sheet with data such as name, level,
//                   current exp, etc.
//      - spellbook.txt: Currently known spells by the character
//      - inventory.txt: Current equipment and data for items in inventory

#include "files.h"

// fprintf doesn't support QString, so we convert the contents of the qstring
// to a character pointer which we can print using printf's %s.
void qsprintln(QString string) {
    printf("%s\n", string.toLocal8Bit().data());
}

// Create a playercharacter struct. Open the file, check that it exists, modify
// the value of the player's variables from the character sheet file. "#" are
// comments, leading and trailing whitespace is ignored.
playercharacter loadsheet(QString path) {
    struct playercharacter player;
    player.name="";
    QFile file(path);
    if(!file.open((QIODevice::ReadOnly))) {
        qWarning("Couldn't open character sheet file");
        return player;
    }

    QTextStream in(&file);

    while(!in.atEnd()) {
        QString line = in.readLine();
        if(!(line.at(0)=="#")) {
            QStringList args = line.split("=");
            QString token = args[0].trimmed();
            QString val = args[1].trimmed();
            if(token=="name") {
                player.name = val;
            } else if(token=="iclass") {
                int z = val.toInt();
                if(z<4) {
                    player.superclass = (superclasses)z;
                }
            } else if(token=="level") {
                if(val.toInt()<1) {
                    player.level = 1;
                } else if(val.toInt()>21) {
                    player.level = 21;
                } else {
                    player.level = val.toInt();
                }
            }
        }
    }

    file.close();

    return player;
}

void loadstats(QString path, playercharacter* player) {
    QString target = "";
    switch(player->superclass) {
        case wizard:
            target="wizard-stats.txt";
        case warrior:
            NULL;
        case rogue:
            NULL;
        case priest:
            NULL;
    }

    QFile file(path + "/" + target);

    if(!file.open((QIODevice::ReadOnly))) {
        qWarning("Couldn't open character sheet file");
        return;
    }

    QTextStream in(&file);

    while(!in.atEnd()) {
        QString line = in.readLine();
        if(!(line.at(0)=="#")) {
            QStringList args = line.split(QRegularExpression("\\s+"), QString::SkipEmptyParts);
            QString token = args[0].trimmed();
            int l = args.size();
            if(l>22) {
                l = 21;
            }
            if(token=="thac0") {
                for(int i=0;i<l;i++) {
                    player->thaco[i] = args.at(i).toInt();
                }
            } else if(token=="ppdm") {
                for(int i=0;i<l;i++) {
                    player->ppdm[i] = args.at(i).toInt();
                }
            } else if(token=="rsw") {
                for(int i=0;i<l;i++) {
                    player->rsw[i] = args.at(i).toInt();
                }
            } else if(token=="pp") {
                for(int i=0;i<l;i++) {
                    player->pp[i] = args.at(i).toInt();
                }
            } else if(token=="bw") {
                for(int i=0;i<l;i++) {
                    player->bw[i] = args.at(i).toInt();
                }
            } else if(token=="spell") {
                for(int i=0;i<l;i++) {
                    player->spell[i] = args.at(i).toInt();
                }
            }
        }
    }

    file.close();

}
